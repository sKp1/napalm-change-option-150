#!/usr/bin/env python

import os
import re
import json
from napalm_base import get_network_driver
from pathlib import Path

ip_tftp_actuel = '10.0.0.1'
ip_tftp_nouveau = '10.0.0.2'
driver = get_network_driver('ios')

def get_password(nom_r):
    return 'admin'

def get_status(ip_r):
    send_ping = os.system("ping -c 5 " + ip_r)
    if send_ping == 0:
        print('ping ok')
        return True
    else:
        return False

def get_config(ip_r,d):
    try:
        d.open()
    except:
        print("erreur")
        return None
    conf_r = d.get_config()
    f = open('conf_tmp.txt','w')
    f.write(conf_r["running"])
    f.close()
    d.close()

    f = open('conf_tmp.txt','r')
    for line in f:
        pool_found = re.findall(r'ip dhcp pool', line)
        if pool_found:
            print(pool_found)
            print(line)
            f2 = open('send_conf.txt','w')
            f2.write(line)
            for line in f:
                option_found = re.findall(ip_tftp_actuel, line)
                if option_found:
                    print(option_found)
                    print(line)
                    f2.write("  option 150 ip " + ip_tftp_nouveau)
    f.close()
    f2.close()

def send_config(ip_r,d):
    print("======================")
    my_file = Path("send_conf.txt")
    if my_file.is_file():
        f2 = open('send_conf.txt','r')
        f2_cont = f2.read()
        print(f2_cont)
        d.open()
        d.load_merge_candidate(filename='send_conf.txt')
        d.commit_config()
        d.close()
        f2.close()
        os.remove('send_conf.txt')
        os.remove('conf_tmp.txt')


# MAIN BODY
# lecture fichier routeur
routeurs_fichier = json.load(open('routeurs.txt'))
for majorkey, subdict in routeurs_fichier.items():
    for subkey, value in subdict.items():
        ip_routeur = value
        nom_routeur = subkey

        pass_routeur = get_password(nom_routeur)
        dev = driver(ip_routeur,'admin',pass_routeur)

        if get_status(ip_routeur):
            #config_routeur = get_config(ip_routeur,dev)
            get_config(ip_routeur,dev)
            send_config(ip_routeur,dev)
            for item in routeurs_fichier:
                del item[subkey]

